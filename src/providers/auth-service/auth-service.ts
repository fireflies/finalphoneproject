import { HttpClient } from '@angular/common/http';
// import { RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
// , HttpParams, HttpHeaders
/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
let apiUrl = 'http://otitays.loc/api/auth/';
let url = 'http://otitays.loc/api/creditHistory/';
let urlProfile = 'http://otitays.loc/api/profile/';
let urlCostumer = 'http://otitays.loc/api/vehicleinfo/';
let urlPoints  = 'http://otitays.loc/api/creditHistory/';
let urlList = 'http://otitays.loc/api/creditList/';
let urlHistDate = 'http://otitays.loc/api/';
let urlFood = 'http://otitays.loc/api/';
let urlSnack = 'http://otitays.loc/api/';
let urlDrink = 'http://otitays.loc/api/';
let urlCoffee = 'http://otitays.loc/api/';
@Injectable()
export class AuthServiceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello AuthServiceProvider Provider');
  }
//register
postData(credentials, type){

    let request = this.http.post(apiUrl + type, credentials);
    console.log(request);
    return request.toPromise();
}
  //login
  login(credentials, type){
    // var headers = new Headers();
    // headers.append('Content-Type', 'application/json');
    // headers.append('X-Requested-With', 'XMLHttpRequest' );
    // const requestOptions = new RequestOptions({ headers });
    // let headers = new HttpHeaders();
    // headers = headers.set('Content-Type','application/json').set('X-Requested-With','XMLHttpRequest');
    // let RequestOptions = {headers: headers};
    // console.log(RequestOptions);
    let request = this.http.post(apiUrl + type, credentials);
    console.log(JSON.stringify(request));
    return request.toPromise();
  }
  //points
  showdata(id){

    let request = this.http.get(url + id);

    return request.toPromise();
  }
  //profile
  showProfile(id):Promise<any>{
    // let headers = new HttpHeaders();
    // headers = headers.set('Content-Type','application/json').set('X-Requesed-With','XMLHttpRequest');
    // let RequestOptions = {headers: headers};
    let request = this.http.get(urlProfile + id);

    return request.toPromise();
  }
//vehicle info
showVehicleInfo(id):Promise<any>{

  let request = this.http.get(urlCostumer + id);

  return request.toPromise();
}   
//show points
show_Points(id):Promise<any>{

  let request = this.http.get(urlPoints + id);
  
  return request.toPromise();
}
show_List_Points(id):Promise<any>{

  let request = this.http.get(urlList + id);
  return request.toPromise();
}
showFood(id, type):Promise<any>{

  let request = this.http.get(urlFood + type + id);

  return request.toPromise();
}
showDrink(id, type):Promise<any>{

  let request = this.http.get(urlDrink + type + id);

  return request.toPromise();
}
showSnack(id, type):Promise<any>{

  let request = this.http.get(urlSnack + type + id);

  return request.toPromise();
}
showCoffee(id, type):Promise<any>{

  let request = this.http.get(urlCoffee + type + id);

  return request.toPromise();
}
order(credentials, type):Promise<any>{

  let request = this.http.post(apiUrl + type, credentials)
  return request.toPromise();
}
showPending(id, type):Promise<any>{
  let request = this.http.get(urlCoffee + type + id);

  return request.toPromise();
}
showHistory(id):Promise<any>{
  let request = this.http.get(urlList + id);

  return request.toPromise();
}
showhistdate(id, type){
  let request = this.http.get(urlHistDate + type + id);

  return request.toPromise();
}
}
