import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the MealPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-meal',
  templateUrl: 'meal.html',
})
export class MealPage {
  value:any;
  meal_type = [];
  dates = [];
  history_data = [];
  choice:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public authservice: AuthServiceProvider, private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MealPage');
    this.getHistory();
    this.getDate();
  }
  getHistory(){
    this.storage.get('user_id').then((val)=>{
      this.value = val;
      this.authservice.showHistory(this.value).then(result =>{
          //mao ni akong na EDIT TIWASA NI YAWAAAAA 10/5/2019
          console.log(result.data.length);
          for(var i =0;i< result.data.length; i++){
            this.meal_type[i] = result.data[i];
            console.log(this.meal_type[i]);
          }
      }).catch(function(err){
        console.log(JSON.stringify(err.message));
      });
    });
  }
  getDate(){
    this.storage.get('user_id').then((val)=>{
      this.value = val;
      this.authservice.showhistdate(this.value, 'datehist/').then(res =>{
          //mao ni akong na EDIT TIWASA NI YAWAAAAA 10/5/2019
          console.log(res.data.length);
          for(var i =0;i< res.data.length; i++){
            this.dates[i] = res.data[i];
            console.log(this.dates[i]);
            var makeString = this.meal_type[i] + ' ' + this.dates[i];
            this.history_data[i] = makeString;
            console.log(this.history_data[i]);
          }

      }).catch(function(err){
        console.log(JSON.stringify(err.message));
      });
    });
  }
}
