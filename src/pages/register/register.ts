import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  d = new Date();
  //status must be 0 ayaw kalimot joel please :)
  userData = {"firstname": "", "middlename": "", "lastname": "", "birthday": "",
   "email": "", "password": "","status": "0", "user_type": "","cond_experience":"", "driver_id":"", "phone_number": "",
   "license": "","vehicle_type": "","plate_number": "", "owner_name":"","date_assigned":""};
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthServiceProvider
    , public alert: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  signup(){
    this.authService.postData(this.userData, 'signup').then(result=>{
      console.log(JSON.stringify(result));
      if(result['message'] == 'Driver does not exist!'){
        let alerts = this.alert.create({
          title: "Oops!",
          message:"It seems the driver does not exist!",
          buttons:[
            {
              text:"Okay"
            }]
        });
        alerts.present();
      }else{
        let alerts = this.alert.create({
          title: "Success",
          message:"Successfully Registered!",
          buttons:[
            {
              text:"Okay"
            }]
        });
        alerts.present();
        this.navCtrl.setRoot(LoginPage);
      }
      
    }).catch(function(err){
      console.log('error' +JSON.stringify(err));
    })
   
  }
}
