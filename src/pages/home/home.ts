import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  value:any;
  noPoints:any;
  food:any;
  time:any;
  constructor(public navCtrl: NavController, public authservice: AuthServiceProvider, public navparams: NavParams, private storage: Storage) {

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
    this.viewinfo();
  }

  viewinfo(){
    this.storage.get('user_id').then((val)=>{
      this.value = val;
      this.authservice.showPending(this.value, 'pending/').then(result =>{
          //mao ni akong na EDIT TIWASA NI YAWAAAAA 10/5/2019
          this.food = result['food'];
          this.time = result['date'];
          console.log(this.food);
          if(typeof this.food === null){
            this.noPoints = 'noPoints';
            console.log(this.noPoints);
          }
      }).catch(function(err){
        console.log(JSON.stringify(err.message));
      });
    });
  }

}
