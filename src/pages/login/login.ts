import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import {AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  userData = {"email": "", "password":"", "remember_me": true};
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthServiceProvider, private storage: Storage, private loadingController: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.isset();
    // this.storage.get('access_')
  }
  isset(){
    this.storage.get('access_key').then((val)=>{
      if(val != null){
        console.log(val);
        this.navCtrl.setRoot(HomePage);  
      }
    })
  }
  async login(){
    let loading = await this.loadingController.create({
      spinner: null,
      duration: 5000
    });
    await loading.present();
    this.authService.login(this.userData, 'login').then(result => {
      console.log(result['access_token']);
    
      this.storage.set('access_key', result['access_token']);
      this.storage.set('user_id', result['user_id']);
      this.navCtrl.setRoot(HomePage);
    }).catch(function(err){
      console.log(JSON.stringify(err));
    }); 
    // this.navCtrl.setRoot(HomePage);
  }
  register(){
    this.navCtrl.push(RegisterPage);
  }

}
