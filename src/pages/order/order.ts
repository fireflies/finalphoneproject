import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the OrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {
  userOrder= {"patron_id": "","food": "", "drink": ""};
  foods = [];
  drunks = [];
  snacks = [];
  coffees = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,
     public authService: AuthServiceProvider, public alert: AlertController, private storage: Storage) {
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderPage');
    this.food();
    this.drink();
    this.snack();
    this.coffee();
  }

  food(){
    var id = 1;
    var type = 'food/'
    this.authService.showFood(id, type).then(result=>{
      console.log(result.data.length);
      for(var i =0;i <= result.data.length;++i){
          this.foods[i] = result.data[i].name;
          console.log(this.foods[i]);
        
      }
    }).catch(function(err){
      console.log(JSON.stringify(err));
      
    });
  }
  drink(){
    var id = 1;
    var type = 'drink/'
    this.authService.showDrink(id, type).then(result=>{
      console.log(result.data.length);
      for(var i =0;i <= result.data.length;++i){
        this.drunks[i] = result.data[i].name;
        console.log(this.drunks[i]);
      }
    }).catch(function(err){
      console.log(JSON.stringify(err));
    })
  }
  snack(){
    var id = 1;
    var type = 'snack/'
    this.authService.showDrink(id, type).then(result=>{
      console.log(result.data.length);
      for(var i =0;i <= result.data.length;++i){
        this.snacks[i] = result.data[i].name;
        console.log(this.snacks[i]);
      }
    }).catch(function(err){
      console.log(JSON.stringify(err));
    })
  }
  coffee(){
    var id = 1;
    var type = 'coffee/'
    this.authService.showCoffee(id, type).then(result=>{
      console.log(result.data.length);
      for(var i =0;i <= result.data.length;++i){
        this.coffees[i] = result.data[i].name;
        console.log(this.coffees[i]);
      }
    }).catch(function(err){
      console.log(JSON.stringify(err));
    })
  }
  order(){
    this.storage.get('user_id').then((val)=>{
      this.userOrder.patron_id = val.toString();
      console.log(this.userOrder.patron_id);
      this.authService.postData(this.userOrder, 'insertOrder').then(result=>{
        console.log(JSON.stringify(result));
        if(result['message'] == '1'){
          let alerts = this.alert.create({
            title: "Oops!",
            message:"It seems that you've already ordered!",
            buttons:[
              {
                text:"Okay"
              }]
          });
          alerts.present();
          this.navCtrl.setRoot(HomePage);
        }
        if(result['message'] == '2'){
          let alerts = this.alert.create({
            title: "Success",
            message:"Successfully Ordered!",
            buttons:[
              {
                text:"Okay"
              }]
          });
          alerts.present();
          this.navCtrl.setRoot(HomePage);
        }
        
      }).catch(function(err){
        console.log('error' +JSON.stringify(err));
      })
    })
    
  }

}
