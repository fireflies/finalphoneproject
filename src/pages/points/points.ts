import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the PointsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-points',
  templateUrl: 'points.html',
})
export class PointsPage {
  value:any;
  points:any;
  passenger:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthServiceProvider, private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PointsPage');
    this.getPoints();
    this.get_List_Points();
  }
  getPoints(){
    this.storage.get('user_id').then((val)=>{
      this.value = val;
      this.authService.show_Points(this.value).then(result =>{
          //mao ni akong na EDIT TIWASA NI YAWAAAAA 10/5/2019
          this.value = result.message;
          console.log(this.value);
          if(typeof this.value === 'undefined'){
          }
      }).catch(function(err){
        console.log(JSON.stringify(err.message));
      });
    });

  }

  get_List_Points(){
    this.storage.get('user_id').then((val)=>{
      this.value = val;
      this.authService.show_List_Points(this.value).then(result =>{
          //mao ni akong na EDIT TIWASA NI YAWAAAAA 10/5/2019
          
          console.log(result.data.length);
          for(let i = 0; i <= result.data.length; i++){
            console.log('hi');
            this.points = result.data[i].points_earned;
            this.passenger = result.data[i].no_of_passenger;
            console.log(this.points);
            console.log(this.passenger);
          }
          if(typeof this.value === 'undefined'){
          }
      }).catch(function(err){
        console.log(JSON.stringify(err.message));
      });
    });
  
  }

}
