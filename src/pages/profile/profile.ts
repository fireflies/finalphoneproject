import { Component } from '@angular/core';
import {AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  user_info = {"firstname": "", "middlename":"","lastname":"","birthday":"","email":""};
  vehicle_info = {"vehicle_Type": "", "plate_number": "", "owner_name": ""};
  id:any;
  noInternet:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthServiceProvider, private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    this.getinfo();
    this.getVehicleData();
  }
  getinfo(){
    this.storage.get('user_id').then((val)=>{
      this.id = val;
      this.authService.showProfile(this.id).then(result=>{
        console.log(result.data[0].middlename);

        this.user_info.firstname = result.data[0].firstname;
        
        this.user_info.middlename = result.data[0].middlename;
        
        this.user_info.lastname = result.data[0].lastname;
        
        this.user_info.birthday = result.data[0].birthdate;
        
        this.user_info.email = result.data[0].email;
      
      }).catch(function(err){
        console.log(JSON.stringify(err));
        
      });

    });
  }
  getVehicleData(){
    this.storage.get('user_id').then((val)=>{
      this.id = val;
      this.authService.showVehicleInfo(this.id).then(result=>{
        console.log(result.data[0].vehicle_type);

        this.vehicle_info.vehicle_Type = result.data[0].vehicle_type;
        
        this.vehicle_info.plate_number = result.data[0].plate_number;
        
        this.vehicle_info.owner_name = result.data[0].owner_name;
        
      }).catch(function(err){
        console.log(JSON.stringify(err));
        
      });

    });

  }

}
